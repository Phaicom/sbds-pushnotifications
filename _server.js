var admin = require("firebase-admin");

var serviceAccount = require("./key/serviceAccountKey.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://io2016bkk-f9429.firebaseio.com"
});

// Define a condition which will send to devices which are subscribed
// to either the Google stock or the tech industry topics.
var condition = "'news' in topics";

// See the "Defining the message payload" section below for details
// on how to define a message payload.
var payload = {
  notification: {
    title: "$GOOG up 1.43% on the day",
    body: "$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day."
  }
};

// Send a message to devices subscribed to the combination of topics
// specified by the provided condition.
admin.messaging().sendToCondition(condition, payload)
  .then(function(response) {
    // See the MessagingConditionResponse reference documentation for
    // the contents of response.
    console.log("Successfully sent message:", response);
  })
  .catch(function(error) {
    console.log("Error sending message:", error);
  });