'use strict';

module.exports = function (app) {
  var notification = require('../controllers/androidController');

  // notification Routes
  app.route('/api/notification')
    .post(notification.push_notification);
};