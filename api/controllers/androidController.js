'use strict';

var admin = require("firebase-admin");
var serviceAccount = require("../../key/serviceAccountKey.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://io2016bkk-f9429.firebaseio.com"
});

var condition = "'new' in topics";

var payload = {
    notification: {
        title: "$GOOG up 1.43% on the day",
        body: "$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day."
    }
};

var options = {
    priority: "high",
    timeToLive: 60 * 60 * 24
};

exports.push_notification = function (req, res) {
    let data = req.body;
    if (data.condition !== null) {
        condition = data.condition;
    }
    payload = data.payload;
    admin.messaging().sendToCondition(condition, payload, options)
        .then(function (response) {
            console.log("Successfully sent message:", response, req.body);
            res.send('push_notification request to android success');
        })
        .catch(function (error) {
            console.log("Error sending message:", error);
        });
};